#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment

podman run \
    --env-file=$ENV_FILE \
    --volume=ckan_database:/var/lib/postgresql/data \
    --name=ckan-postgresql \
    --rm \
    --pod ckan-pod \
    ckan-postgresql
