# Esquemas

Con archivos de este módulo es posible configurar los formularios de creación y
edición de diferentes componentes clave de CKAN. A decir: el dataset, la
organización y el grupo.

ver [aquí](https://github.com/ckan/ckanext-scheming/) para más información.
