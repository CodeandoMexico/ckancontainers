#!/bin/sh
set -e

UWSGI_FILE=${CKAN_CONFIG}/uwsgi.ini
WHO_CONFIG_FILE="$CKAN_CONFIG/who.ini"

abort () {
  echo "$@" >&2
  exit 1
}

CKAN_SQLALCHEMY_URL=postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/$POSTGRES_DB
CKAN_DATASTORE_WRITE_URL=postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/$DATASTORE_DB
CKAN_DATASTORE_READ_URL=postgresql://${DATASTORE_READONLY_USER}:${DATASTORE_READONLY_PASSWORD}@${POSTGRES_HOST}/$DATASTORE_DB
CKAN_SOLR_URL=http://${SOLR_HOST}:8983/solr/ckan
CKAN_REDIS_URL=redis://${REDIS_HOST}:6379/1
CKAN_DATAPUSHER_URL=http://${DATAPUSHER_HOST}:8800

CLEAN_SITE_URL=$(echo $CKAN_SITE_URL | sed "s/\//\\\\\//g")
CLEAN_CKAN_SQLALCHEMY_URL=$(echo $CKAN_SQLALCHEMY_URL | sed "s/\//\\\\\//g")
CLEAN_CKAN_DATASTORE_READ_URL=$(echo $CKAN_DATASTORE_READ_URL | sed "s/\//\\\\\//g")
CLEAN_CKAN_DATASTORE_WRITE_URL=$(echo $CKAN_DATASTORE_WRITE_URL | sed "s/\//\\\\\//g")

write_config () {
    echo "Generating config at $CKAN_INI..."
    ckan generate config "$CKAN_INI"
}

# Wait for PostgreSQL
if ! pg_isready -h "${POSTGRES_HOST}" -U "${POSTGRES_USER}"; then
    abort "Postgresql not running"
fi

# If we don't already have a config file, bootstrap
if [ ! -e "$CKAN_INI" ]; then
    write_config

    # Set config values from environment variables, because ckan doesnt do it by
    # itself
    sed -i "s/#solr_url = http:\/\/127.0.0.1:8983\/solr/solr_url = http:\/\/$SOLR_HOST:8983\/solr\/ckan/g" $CKAN_INI
    sed -i "s/#ckan.redis.url = redis:\/\/localhost:6379\/0/ckan.redis.url = redis:\/\/$REDIS_HOST:6379\/0/g" $CKAN_INI
    sed -i "s/sqlalchemy.url = postgresql:\/\/ckan_default:pass@localhost\/ckan_default/sqlalchemy.url = $CLEAN_CKAN_SQLALCHEMY_URL/g" $CKAN_INI
    sed -i "s/#ckan.storage_path/ckan.storage_path/g" $CKAN_INI
    sed -i "s/ckan.site_url =/ckan.site_url = $CLEAN_SITE_URL/g" $CKAN_INI
    sed -i "s/ckan.plugins = stats text_view image_view recline_view/ckan.plugins = theme stats text_view image_view recline_view datastore datapusher resource_proxy scheming_datasets pdf_view geojson_view geo_view/g" $CKAN_INI
    sed -i "s/ckan.views.default_views = image_view text_view recline_view/ckan.views.default_views = image_view text_view recline_view pdf_view geojson_view geo_view/g" $CKAN_INI
    sed -i "s/#ckan.max_resource_size = 10/ckan.max_resource_size = $CKAN_MAX_RESOURCE_SIZE/g" $CKAN_INI
    sed -i "s/#ckan.preview.json_formats = json/ckan.preview.json_formats = json geojson/g" $CKAN_INI
    sed -i "s/ckan.auth.create_user_via_web = true/ckan.auth.create_user_via_web = false/g" $CKAN_INI

    # datapusher
    sed -i "s/#ckan.datapusher.url = http:\/\/127.0.0.1:8800\//ckan.datapusher.url = http:\/\/$DATAPUSHER_HOST:$DATAPUSHER_PORT/g" $CKAN_INI

    # datastore connection strings
    sed -i "s/#ckan.datastore.read_url = postgresql:\/\/datastore_default:pass@localhost\/datastore_default/ckan.datastore.read_url = $CLEAN_CKAN_DATASTORE_READ_URL/g" $CKAN_INI
    sed -i "s/#ckan.datastore.write_url = postgresql:\/\/ckan_default:pass@localhost\/datastore_default/ckan.datastore.write_url = $CLEAN_CKAN_DATASTORE_WRITE_URL/g" $CKAN_INI

    # email settings
    sed -i "s/#email_to = errors@example.com/email_to = $EMAIL_TO/g" $CKAN_INI
    sed -i "s/#error_email_from = ckan-errors@example.com/error_email_from = $ERROR_EMAIL_FROM/g" $CKAN_INI
    sed -i "s/#smtp.server = localhost/smtp.server = $SMTP_SERVER/g" $CKAN_INI
    sed -i "s/#smtp.starttls = False/smtp.starttls = True/g" $CKAN_INI
    sed -i "s/#smtp.user = username@example.com/smtp.user = $SMTP_USER/g" $CKAN_INI
    sed -i "s/#smtp.password = your_password/smtp.password = $SMTP_PASSWORD/g" $CKAN_INI
    sed -i "s/#smtp.mail_from =/smtp.mail_from = $SMTP_MAIL_FROM/g" $CKAN_INI
    sed -i "s/#smtp.reply_to =/smtp.reply_to = $SMTP_REPLY_TO/g" $CKAN_INI
    sed -i "s/#ckan.activity_streams_email_notifications = true/ckan.activity_streams_email_notifications = true/g" $CKAN_INI
fi

if [ ! -e $UWSGI_FILE ]; then
    cp $CKAN_EXTRA/uwsgi.ini $UWSGI_FILE
fi

if [ ! -e $WHO_CONFIG_FILE ]; then
    cp $CKAN_CODE/ckan/config/who.ini $WHO_CONFIG_FILE
fi

# Get or create CKAN_SQLALCHEMY_URL
if [ -z "$CKAN_SQLALCHEMY_URL" ]; then
  abort "ERROR: no CKAN_SQLALCHEMY_URL specified"
fi

if [ -z "$CKAN_SOLR_URL" ]; then
    abort "ERROR: no CKAN_SOLR_URL specified"
fi

if [ -z "$CKAN_REDIS_URL" ]; then
    abort "ERROR: no CKAN_REDIS_URL specified"
fi

if [ -z "$CKAN_DATAPUSHER_URL" ]; then
    abort "ERROR: no CKAN_DATAPUSHER_URL specified"
fi

# initialize DB
ckan -c $CKAN_INI db init

ckan -c $CKAN_INI datastore set-permissions > /tmp/perms.sql
PGPASSWORD=$POSTGRES_PASSWORD psql -U $POSTGRES_USER -h $POSTGRES_HOST --set ON_ERROR_STOP=1 < /tmp/perms.sql
rm /tmp/perms.sql

# run ckan with uwsgi
exec $CKAN_VENV/bin/uwsgi -i $UWSGI_FILE
