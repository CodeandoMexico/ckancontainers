#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment
image=ckan

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=ckan"
else
    cmd="-it $image $@"
fi

podman run \
    --env-file=$ENV_FILE \
    --volume=$__dir/../storage/config/ckan:/etc/ckan/ckan:U \
    --volume=$__dir/../storage/data:/var/lib/ckan:U \
    $name \
    --rm \
    --pod ckan-pod \
    $cmd

# Some volumes that were useful at some point
#
# * To hack or debug on ckan code itself:
#
# --volume=/path/to/ckan/ckan:/opt/ckan/code/ckan \
#
#   In that case this might be required for ckan not to complain about a
#   readonly directory:
#
# --volume=ckan_i18n:/opt/ckan/code/ckan/public/base/i18n \
#
# * And this for extension development (like the theme for example)
#
# --volume=$__dir/ckanext-<name>/ckanext:/opt/ckan/plugins/ckanext-<name>/ckanext:rw \
#
# * Or for schema development:
#
# --volume=$__dir/schemas/customschemas:/opt/ckan/plugins/schemas/customschemas:rw \
# --volume=/path/to/ckanext-scheming/ckanext/scheming:/opt/ckan/venv/src/ckanext-scheming/ckanext/scheming:ro \
