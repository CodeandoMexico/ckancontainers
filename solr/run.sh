#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment

podman run \
    --env-file=$ENV_FILE \
    --volume=ckan_solr_data:/opt/solr/server/solr/ckan/data \
    --name=ckan-solr \
    --rm \
    --pod ckan-pod \
    ckan-solr
