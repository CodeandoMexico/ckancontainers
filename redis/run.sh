#!/bin/bash

podman run \
    --name=ckan-redis \
    --rm \
    --pod ckan-pod \
    redis:6.2
