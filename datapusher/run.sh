#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../environment
image=ckan-datapusher

if [[ -z $@ ]]; then
    cmd=$image
else
    cmd="-it $image $@"
fi

podman run \
    --env-file=$ENV_FILE \
    --volume=$__dir/../storage/config/datapusher:/etc/ckan/datapusher:U \
    --name=ckan-datapusher \
    --rm \
    --pod ckan-pod \
    $cmd

# Useful volumes for development
# --volume=/path/to/datapusher-plus/datapusher:/opt/ckan/code/datapusher:ro \
# --volume=/path/to/datapusher-plus/deployment:/opt/ckan/code/deployment:ro \
# --volume=/path/to/ckan-service-provider/ckanserviceprovider:/opt/ckan/venv/lib/python3.8/site-packages/ckanserviceprovider:ro \
