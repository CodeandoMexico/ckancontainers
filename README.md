# Instalación con contenedores de CKAN

En este repositorio están las instrucciones y Dockerfiles necesarias para una
instalación base de CKAN con algunos módulos básicos.

Cada imagen que se construye aquí tiene su propio script `run.sh` para pruebas
locales y un archivo `.service` para producción, así como un `build.sh` con un
ejemplo de construcción de la imagen.

## ¿Por qué?

La motivación para esto es la portabilidad de una instalación de CKAN, así como
simplificar su administración y mantenimiento. Las imágenes de contenedor
aseguran que, una vez generadas, es posible levantarlas en cualquier sistema
operativo con soporte de contenedores e incluso hacer migraciones de servidor
sin mayor complicación.

## Construicción de las imágenes

Cada servicio aquí usado tiene su propio `build.sh` y `Containerfile` excepto
redis, del cual se usa la imagen oficial. Para constriur cada uno basta con
hacer:

    ./postgresql/build.sh
    ./solr/build.sh
    ./ckan/build.sh

En el caso de redis basta con bajar la imagen:

    podman pull docker.io/redis:6.2

## Configuración

La ejecución de los diferentes servicios requiere un único archivo de
configuración que concentra todos los parámetros necesarios para su
inicialización. Después de la primera ejecución es posible continuar la
configuración haciendo uso de los archivos específicos de cada servicio.

Para comenzar hay que copiar el archivo `example.env` en un archivo
`environment` esperado por los diferentes scripts de ejecución y ajustar el
contenido del mismo.

**Advertencia** Por la forma en que algunas variables son reemplazadas en el
archivo de configuración existe cierta sensibilidad a la presencia del caracter
`'/'` en los valores, así que es mejor evitarlo.

## Levantar los servicios

Primero hay que crear (o asegurar que exista) el pod. Este es el "host" que une
a todos los servicios para que se puedan localizar entre sí en localhost:

    ./pod/run.sh

Luego se pueden levantar los diferentes contenedores que integran el pod:

    ./postgresql/run.sh
    ./redis/run.sh
    ./solr/run.sh

Finalmente se puede levantar el servicio de ckan y datapusher:

    ./ckan/run.sh
    ./datapusher/run.sh

## Ejecutar comandos de CKAN

Hay en la raíz del proyecto un script ejecutable `ckan.sh` que simplifica la
ejecución de comandos dentro del contenedor.

Por ejemplo para crear un usuario sysadmin:

    ./ckan.sh sysadmin add <username> email=<email> name=<username>

O iniciar un worker para tareas en segundo plano:

    ./ckan.sh jobs worker

## Creación de una extensión

Para crear una nueva extensión hay que crear primero la carpeta local en la que
va a vivir:

    mkdir tmp

luego modificar temporalmente el archivo `ckan.sh` para montar un volumen en esa
carpeta

    --volume=$__dir/tmp:/opt/ckan/tmp:rw \

y generarla:

    ./ckan.sh generate extension -o /opt/ckan/tmp

Ahora necesitarás cambiar la dueña de la nueva extensión a tu propio usuario:

    sudo chown -R tu:tu tmp

y mover la extensión generada a su ubicación junto a las demás:

    mv tmp/ckanext-whatever ckan/

En este punto lo que sigue es editar el Containerfile para integrar la nueva
extensión como se hace con las demás y modificar el `ckan.ini` almacenado en el
volumen de configuración para cargarla. Será necesario reconstruir el
contenedor.

Para el desarrollo de la extensión lo ideal es montar la carpeta de la misma en
nuestra computadora como un volumen dentro del contenedor en la ubicación a la
que se copió (seguramente dentro de `/opt/ckan/plugins`) como sigue:

    --volume=$__dir/ckanext-filterprivate/ckanext:/opt/ckan/plugins/ckanext-filterprivate/ckanext:rw \

## Enviar notificaciones

configurar un token de sysadmin como en la guía y ejecutar:

    curl -H "$CKAN_AUTH_HEADER" -X POST -s -d "{}" http://localhost:5000/api/action/send_email_notifications

se puede hace dentro de un contenedor con curl como en:

    podman run --rm --name ckan-notification-emails --pod ckan-pod curl -H "$CKAN_AUTH_HEADER" -X POST -s -d "{}" http://localhost:5000/api/action/send_email_notifications

y se puede configurar como timer de systemd como en la guía.

## Podman

Para recargar el contenedor de CKAN sin reiniciarlo y volverlo a iniciar a
manita:

    kill -s HUP $(podman inspect ckan | jq .[0].State.Pid)

requere de [jq](https://stedolan.github.io/jq/)

### Creación de los archivos de systemd

Levantar todos los contenedores como arriba y luego correr:

    podman generate systemd --files --name --new --pod-prefix= --container-prefix= ckan-pod

los archivos quedarjań en la carpeta actual. Será necesario ajustar las rutas a
archivos de configuración y entornos. Una vez movidos al servidor de producción
y atualizadas las unidades con `systemctl --user daemon-reload` es posible
iniciar todos los servicios con

    systemctl --user start ckan-pod

así mismo es posible detenerlos todos con

    systemctl --user stop ckan-pod
