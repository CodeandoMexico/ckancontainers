#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/environment

podman run \
    --env-file=$ENV_FILE \
    --volume=$__dir/storage/config/ckan:/etc/ckan/ckan:U \
    --volume=$__dir/storage/data:/var/lib/ckan:U \
    --rm \
    --pod ckan-pod \
    -it ckan ckan -c /etc/ckan/ckan/ckan.ini $@
